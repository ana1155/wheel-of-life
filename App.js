import { Navigation } from 'react-native-navigation'
import { Provider } from 'react-redux'

import AuthScreen from './src/screens/Auth'
import Wheel from './src/screens/Wheel'
import Settings from './src/screens/Settings'
import EnterResults from './src/screens/EnterResults'
import SphereDetail from './src/screens/SphereDetail'
import SideDrawer from './src/screens/SideDrawer'
import configureStore from './src/store/configureStore'

const store = configureStore()

//  register screens
Navigation.registerComponent('WheelOfLife.AuthScreen', () => AuthScreen)
Navigation.registerComponent('WheelOfLife.WheelScreen', () => Wheel, store, Provider)
Navigation.registerComponent('WheelOfLife.SettingsScreen', () => Settings, store, Provider)
Navigation.registerComponent('WheelOfLife.EnterResultsScreen', () => EnterResults, store, Provider)
Navigation.registerComponent('WheelOfLife.SphereDetailScreen', () => SphereDetail, store, Provider)
Navigation.registerComponent('WheelOfLife.SideDrawer', () => SideDrawer)

//  Start the App
Navigation.startSingleScreenApp({
  screen: {
    screen: 'WheelOfLife.AuthScreen',
    title: 'Login',
  },
})
