import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { selectSphere } from '../../store/reducers/resultsReducer'

type Props = {
  itemSelected: Function,
  sphere: string,
}

const ListItem = ({ sphere, itemSelected }: Props) => (
  <TouchableOpacity style={styles.listItem} onPress={() => itemSelected(sphere.key)}>
    <Text style={styles.text}>{sphere.name}</Text>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  listItem: {
    width: '98%',
    height: 30,
    margin: 3,
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#474747',
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#373737',
  },
})

const mapDispatchToProps = dispatch => ({
  selectThis: name => dispatch(selectSphere(name)),
})

export default connect(null, mapDispatchToProps)(ListItem)
