import React from 'react'
import { FlatList, StyleSheet, Button, View } from 'react-native'
import { connect } from 'react-redux'
import { applySpec } from 'ramda'

import ListItem from '../ListItem'
import { getSpheresState } from '../../store/reducers/resultsReducer'

type Props = {
  spheres: Array<Object>,
  itemSelected: Function,
}

const List = ({ spheres, itemSelected }: Props) => (
  <View style={styles.view}>
    <FlatList
      style={styles.list}
      data={spheres}
      renderItem={info => <ListItem sphere={info.item} itemSelected={itemSelected} />}
    />
    <Button title="Submit" onPress={() => {}} color="green" />
  </View>
)

const styles = StyleSheet.create({
  view: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  list: {
    width: '100%',
  },
})

const mapStateToProps = applySpec({
  spheres: getSpheresState,
})

export default connect(mapStateToProps)(List)
