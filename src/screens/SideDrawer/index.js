import React from 'react'
import { View, Text, Dimensions, StyleSheet } from 'react-native'

const SideDrawer = () => (
  <View style={[styles.container, { width: Dimensions.get('window').width * 0.8 }]}>
    <Text>Side menu</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    backgroundColor: 'white',
    flex: 1,
  },
})

export default SideDrawer
