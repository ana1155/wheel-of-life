import React from 'react'
import { View, Modal, TextInput, Button, Text, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { applySpec } from 'ramda'
import {
  addComment,
  addScore,
  getCommentSelectedState,
  getScoreSelectedState,
  getSphereSelectedState,
} from '../../store/reducers/resultsReducer'

type Props = {
  onClose: Function,
  editScore: Function,
  editComment: Function,
  onSubmit: Function,
  sphereSelected: Object,
  score: number,
  comment: string,
}

const SphereDetail = ({
  sphereSelected,
  onClose,
  score,
  comment,
  editScore,
  editComment,
  onSubmit,
}: Props) => (
  <Modal visible={!!sphereSelected} onRequestClose={onClose} animationType="slide">
    <View style={styles.view}>
      <Text style={styles.sphere}>{sphereSelected}</Text>
      <TextInput
        value={score}
        placeholder="Score points this week"
        onChangeText={editScore}
        style={styles.input}
      />
      <TextInput
        value={comment}
        placeholder="Add some comment..."
        multiline
        onChangeText={editComment}
        style={styles.input}
      />
      <View style={styles.buttons}>
        <Button title="Close" onPress={onClose} color="red" style={styles.button} />
        <Button title="Submit" onPress={onSubmit} style={styles.button} />
      </View>
    </View>
  </Modal>
)

const styles = StyleSheet.create({
  view: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 30,
  },
  sphere: {
    fontWeight: 'bold',
  },
  input: {
    width: '80%',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-around',
    marginTop: '20%',
  },
  button: {
    margin: 5,
  },
})

const mapStateToProps = applySpec({
  sphereSelected: getSphereSelectedState,
  score: getScoreSelectedState,
  comment: getCommentSelectedState,
})

const mapDispatchToProps = dispatch => ({
  editScore: value => dispatch(addScore(value)),
  editComment: value => dispatch(addComment(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SphereDetail)
