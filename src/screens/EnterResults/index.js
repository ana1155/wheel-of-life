import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { applySpec } from 'ramda'

import List from '../../components/List'
import {
  deselectSphere,
  getSpheresState,
  selectSphere,
  submitChanges,
} from '../../store/reducers/resultsReducer'

type Props = {
  selectThis: Function,
  onClose: Function,
  onSubmit: Function,
  navigator: Object,
  spheres: Array<Object>,
}

class EnterResults extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent)
  }

  onNavigatorEvent = (event) => {
    if (event.type === 'NavBarButtonPress' && event.id === 'sideDrawerToggle') {
      this.props.navigator.toggleDrawer({
        side: 'left',
      })
    }
  }

  render() {
    const itemSelectedHandler = (key: string) => {
      const sphereSelected = this.props.spheres.find((sphere: Object) => sphere.key === key)
      this.props.selectThis(sphereSelected)
      const closeView = () => {
        this.props.onClose()
        this.props.navigator.pop({
          animated: true,
          animationType: 'fade',
        })
      }
      const submit = () => {
        this.props.onSubmit()
        this.props.navigator.pop({
          animated: true,
          animationType: 'fade',
        })
      }
      this.props.navigator.push({
        screen: 'WheelOfLife.SphereDetailScreen',
        title: sphereSelected.name,
        passProps: {
          onClose: closeView,
          onSubmit: submit,
        },
      })
    }
    return (
      <View>
        <Text>Enter your results for this period here</Text>
        <List itemSelected={itemSelectedHandler} />
      </View>
    )
  }
}

const mapStateToProps = applySpec({
  spheres: getSpheresState,
})

const mapDispatchToProps = dispatch => ({
  selectThis: sphere => dispatch(selectSphere(sphere)),
  onClose: () => dispatch(deselectSphere()),
  onSubmit: () => dispatch(submitChanges()),
})

export default connect(mapStateToProps, mapDispatchToProps)(EnterResults)
