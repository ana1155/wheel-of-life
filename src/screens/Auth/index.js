import React, { Component } from 'react'
import { View, Text, Button, StyleSheet, ImageBackground } from 'react-native'

import startMainTabs from '../MainTabs/startMainTabs'
import DefaultInput from '../../components/UI/DefaultInput'
import backgroundImage from '../../assets/images/4633167343.jpg'

class AuthScreen extends Component {
  loginHandler = () => {
    startMainTabs()
  }

  render() {
    return (
      <ImageBackground source={backgroundImage} style={styles.background}>
        <View style={styles.container}>
          <ImageBackground>
            <Text style={styles.heading}>Please Log In</Text>
            <Button title="Switch to Login" />
            <View style={styles.inputContainer}>
              <DefaultInput placeholder="Your E-Mail" />
              <DefaultInput placeholder="Password" />
              <DefaultInput placeholder="Confirm Password" />
            </View>
            <Button title="Submit" onPress={this.loginHandler} />
          </ImageBackground>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    width: '80%',
  },
  heading: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'black',
  },
  background: {
    flex: 1,
  },
})

export default AuthScreen
