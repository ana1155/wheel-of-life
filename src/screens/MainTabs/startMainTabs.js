import { Navigation } from 'react-native-navigation'
import Icon from 'react-native-vector-icons/Ionicons'

const startTabs = () => {
  Promise.all([
    Icon.getImageSource('ios-aperture-outline', 30),
    Icon.getImageSource('ios-pie-outline', 30),
    Icon.getImageSource('md-settings', 30),
    Icon.getImageSource('md-menu', 30),
  ]).then((sources: Array) => {
    Navigation.startTabBasedApp({
      tabs: [
        {
          screen: 'WheelOfLife.EnterResultsScreen',
          title: 'Results',
          label: 'Results',
          icon: sources[1],
          navigatorButtons: {
            leftButtons: [
              {
                icon: sources[3],
                title: 'Menu',
                id: 'sideDrawerToggle',
              },
            ],
          },
        },
        {
          screen: 'WheelOfLife.WheelScreen',
          title: 'Wheel Of Life',
          label: 'Wheel',
          icon: sources[0],
          navigatorButtons: {
            leftButtons: [
              {
                icon: sources[3],
                title: 'Menu',
                id: 'sideDrawerToggle',
              },
            ],
          },
        },
        {
          screen: 'WheelOfLife.SettingsScreen',
          title: 'Settings',
          label: 'Settings',
          icon: sources[2],
          navigatorButtons: {
            leftButtons: [
              {
                icon: sources[3],
                title: 'Menu',
                id: 'sideDrawerToggle',
              },
            ],
          },
        },
      ],
      drawer: {
        left: {
          screen: 'WheelOfLife.SideDrawer',
        },
      },
    })
  })
}

export default startTabs
