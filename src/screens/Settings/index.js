import React, { Component } from 'react'
import { View, Text } from 'react-native'

class Settings extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent)
  }

  onNavigatorEvent = event => {
    if (event.type === 'NavBarButtonPress' && event.id === 'sideDrawerToggle') {
      this.props.navigator.toggleDrawer({
        side: 'left',
        animated: 'true',
      })
    }
  }

  render() {
    return (
      <View>
        <Text>You can modify your settings here</Text>
      </View>
    )
  }
}

export default Settings
