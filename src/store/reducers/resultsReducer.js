import { createAction, handleActions } from 'redux-actions'
import { createSelector } from 'reselect'
import { merge } from 'ramda'

export const selectSphere = createAction('SELECT_SPHERE')
export const deselectSphere = createAction('DESELECT_SPHERE')
export const addScore = createAction('ADD_SCORE')
export const addComment = createAction('ADD_COMMENT')
export const submitChanges = createAction('SUBMIT_CHANGES')

const getSpheres = state => state.results.spheres
const getSphereSelected = state => state.results.sphereSelected
const getScoreSelected = state => state.results.scoreSelected
const getCommentSelected = state => state.results.commentSelected

export const getSpheresState = createSelector([getSpheres], spheres => spheres)
export const getSphereSelectedState = createSelector(
  [getSphereSelected],
  sphereSelected => sphereSelected,
)
export const getScoreSelectedState = createSelector(
  [getScoreSelected],
  scoreSelected => scoreSelected,
)
export const getCommentSelectedState = createSelector(
  [getCommentSelected],
  commentSelected => commentSelected,
)

const defaultState = {
  spheres: [
    {
      key: '1',
      name: 'FAMILY',
      score: '',
      comment: '',
    },
    {
      key: '2',
      name: 'SPORT',
      score: '',
      comment: '',
    },
    {
      key: '3',
      name: 'CAREER',
      score: '',
      comment: '',
    },
    {
      key: '4',
      name: 'HEALTH',
      score: '',
      comment: '',
    },
    {
      key: '5',
      name: 'TRAVELS',
      score: '',
      comment: '',
    },
  ],
  sphereSelected: null,
  scoreSelected: null,
  commentSelected: null,
}

const selectSphereHandler = (state, payload) =>
  merge(state, {
    sphereSelected: payload.name,
    scoreSelected: payload.score,
    commentSelected: payload.comment,
  })

const deselectSphereHandler = (state: Object) =>
  merge(state, {
    sphereSelected: null,
    scoreSelected: null,
    commentSelected: null,
  })

const addScoreHandler = (state, payload) => merge(state, { scoreSelected: payload })

const addCommentHandler = (state, payload) => merge(state, { commentSelected: payload })

const submitHandler = (state: Object) => {
  const oldSpheres = [...state.spheres]
  const updatedSpheres = oldSpheres.map((sphr: Object) => {
    let newSphere = sphr
    if (sphr.name === state.sphereSelected) {
      newSphere = merge(sphr, { score: state.scoreSelected, comment: state.commentSelected })
    }
    return newSphere
  })
  return merge(state, {
    spheres: updatedSpheres,
    sphereSelected: null,
    scoreSelected: null,
    commentSelected: null,
  })
}

const resultsReducer = handleActions(
  {
    [selectSphere]: (state, action) => selectSphereHandler(state, action.payload),
    [deselectSphere]: state => deselectSphereHandler(state),
    [addScore]: (state, action) => addScoreHandler(state, action.payload),
    [addComment]: (state, action) => addCommentHandler(state, action.payload),
    [submitChanges]: state => submitHandler(state),
  },
  defaultState,
)

export default resultsReducer
