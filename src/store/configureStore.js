import { createStore, combineReducers, compose } from 'redux'

import resultsReducer from './reducers/resultsReducer'

const rootReducer = combineReducers({
  results: resultsReducer,
})

let composeEnhancers = compose

// eslint-disable-next-line no-undef
if (__DEV__) {
  // eslint-disable-next-line no-underscore-dangle
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

const configureStore = () => createStore(rootReducer, composeEnhancers())

export default configureStore
