import { createAction } from 'redux-actions'
import reducer, { submitChanges } from '../reducers/resultsReducer'

describe('results reducer', () => {
  it('should write score and comment into state', () => {
    const initialState = {
      spheres: [
        {
          key: 1,
          name: 'family',
          score: '',
          comment: '',
        },
        {
          key: 2,
          name: 'sport',
          score: '',
          comment: '',
        },
        {
          key: 3,
          name: 'caree',
          score: '',
          comment: '',
        },
        {
          key: 4,
          name: 'health',
          score: '',
          comment: '',
        },
        {
          key: 5,
          name: 'travels',
          score: '',
          comment: '',
        },
      ],
      sphereSelected: 'family',
      scoreSelected: 10,
      commentSelected: 'cool',
    }
    const action = createAction(submitChanges)
    const result = {
      spheres: [
        {
          key: 1,
          name: 'family',
          score: 10,
          comment: 'cool',
        },
        {
          key: 2,
          name: 'sport',
          score: '',
          comment: '',
        },
        {
          key: 3,
          name: 'caree',
          score: '',
          comment: '',
        },
        {
          key: 4,
          name: 'health',
          score: '',
          comment: '',
        },
        {
          key: 5,
          name: 'travels',
          score: '',
          comment: '',
        },
      ],
      sphereSelected: null,
      scoreSelected: null,
      commentSelected: null,
    }
    expect(reducer(initialState, action())).toEqual(result, initialState)
  })
})
